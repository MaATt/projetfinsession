#FROM ubuntu:19.10

#LABEL email="amelie.t.g@hotmail.ca"
#LABEL author="Amelie Tremblay Gagnon"
#LABEL version="1.0.1"

#ENV DOTNET_VERSION="3.1"

#RUN apt-get update
#RUN apt-get install wget -y

#RUN wget -q https://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
#RUN dpkg -i packages-microsoft-prod.deb

#RUN apt-get install apt-transport-https -y
#RUN apt-get update
#RUN apt-get install dotnet-runtime-3.1 -y
#RUN apt-get update
#RUN apt-get install apt-transport-https -y
#RUN apt-get update
#RUN apt-get install aspnetcore-runtime-3.1 -y
#RUN apt-get update
#RUN apt-get install dotnet-sdk-3.1 -y


#docker pull -t nacyot/cobol-open:apt

FROM ubuntu:19.10 
LABEL author="Amelie Tremblay Gagnon"
LABEL email="amelie.t.g@hotmail.ca"
LABEL version="1.0.1"

RUN apt-get update
RUN apt-get install -y open-cobol

# Set default WORKDIR
WORKDIR /source

